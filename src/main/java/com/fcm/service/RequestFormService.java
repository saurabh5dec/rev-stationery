package com.fcm.service;

import java.util.List;
import java.util.Optional;

import com.fcm.entity.RequestForm;
import com.fcm.entity.RequestFormResponse;
import com.fcm.entity.User;

public interface RequestFormService  {

	public List<RequestForm> findAllData(String id);
	public List<RequestForm> findAllPendingRequest();
	public Optional<RequestForm> findById(Long id);
	public void save(RequestForm requestForm);
	public RequestFormResponse findByEmpId(String empId);
	public RequestFormResponse findAllReport();
//	public List<Quantity> findAllQuantity();
}

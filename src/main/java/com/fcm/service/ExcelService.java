package com.fcm.service;

import java.io.ByteArrayInputStream;

public interface ExcelService {
	public ByteArrayInputStream load();
	public ByteArrayInputStream loadPending();
}

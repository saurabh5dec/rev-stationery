package com.fcm.service;

import java.util.List;
import java.util.Optional;

import com.fcm.entity.Product;
import com.fcm.entity.ProductLog;

public interface ProductService {

	 List<Product> findAll();
	 
	  Product save(Product product);
	  
	  public void deleteById(Long id);
	  public void update(Product product);
		public Optional<Product> findById(Long id);
		public Optional<ProductLog> findByProductLogIdByProductId(Long id);
		public Product edit(Product product);
}


package com.fcm.service;

import java.util.Optional;

import com.fcm.entity.User;

public interface UserService {
	
    Optional<User> findByUserId(Long id);
}

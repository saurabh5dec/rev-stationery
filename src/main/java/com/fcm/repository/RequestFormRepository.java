package com.fcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fcm.entity.RequestForm;

@Repository
public interface RequestFormRepository extends JpaRepository<RequestForm, Long> {

	List<RequestForm> findByemployeeId(String empId);


}

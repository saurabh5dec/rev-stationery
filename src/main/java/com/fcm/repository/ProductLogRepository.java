package com.fcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fcm.entity.ProductLog;

@Repository
public interface ProductLogRepository extends JpaRepository<ProductLog, Long>{

}

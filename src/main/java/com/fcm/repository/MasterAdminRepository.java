package com.fcm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fcm.entity.MasterAdmin;

@Repository
public interface MasterAdminRepository extends JpaRepository<MasterAdmin,Long>{

	MasterAdmin findByBranch(String selectValue);
}

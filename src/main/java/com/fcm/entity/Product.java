package com.fcm.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product {
	   @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name="product_id")
	    private Long id;
	   @OneToMany(cascade = CascadeType.ALL)
		private List<ProductLog> productLog;
       @Column(name="created_on")
	    private String createdOn;
       @Column(name="product_name")
	    private String productName;
       @Column(name="category_type")
	    private String categoryType;
       @Column(name="unit_price")
	    private Long unitPrice;
       @Column(name="updated_by")
	    private String updatedBy;
       @Column(name="updated_on")
	    private Date updatedOn ;
       @Column(name="status")
	    private String  status ;
       @Column(name="quantity")
	    private String  quantity ;
       
       
	   @OneToMany
		private List<RequestForm> requestForm;
	   
//	    @OneToOne(cascade = CascadeType.ALL)
//		private Quantity quantity;

       
       
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public Long getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Long unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<ProductLog> getProductLog() {
		return productLog;
	}
	public void setProductLog(List<ProductLog> productLog) {
		this.productLog = productLog;
	}
	public List<RequestForm> getRequestForm() {
		return requestForm;
	}
	public void setRequestForm(List<RequestForm> requestForm) {
		this.requestForm = requestForm;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
//	public Quantity getQuantity() {
//		return quantity;
//	}
//	public void setQuantity(Quantity quantity) {
//		this.quantity = quantity;
//	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
       
       
}

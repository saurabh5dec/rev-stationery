package com.fcm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stationary_rights_log")
public class StationaryRightsLog {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
//	    @ManyToOne
//	    private Client clients;
	    @Column(name="created_on")
	    private Date createdOn;
	    @Column(name="owner_name")
	    private String ownerName;
        @Column(name="owner_empl_id")
	    private String ownerEmplId;
        @Column(name="branch_assigned")
	    private String branchAssigned;
        @Column(name="rights_type")
	    private String rightsType;
	    @Column(name="updated_by")
	    private String updatedBy;
	    @Column(name="updated_on")
	    private Date updatedOn;
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Date getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
		public String getOwnerName() {
			return ownerName;
		}
		public void setOwnerName(String ownerName) {
			this.ownerName = ownerName;
		}
		public String getOwnerEmplId() {
			return ownerEmplId;
		}
		public void setOwnerEmplId(String ownerEmplId) {
			this.ownerEmplId = ownerEmplId;
		}
		public String getBranchAssigned() {
			return branchAssigned;
		}
		public void setBranchAssigned(String branchAssigned) {
			this.branchAssigned = branchAssigned;
		}
		public String getRightsType() {
			return rightsType;
		}
		public void setRightsType(String rightsType) {
			this.rightsType = rightsType;
		}
		public String getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}
		public Date getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}
	    
	    
}

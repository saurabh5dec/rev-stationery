package com.fcm.entity;

import java.util.List;

public class Quantity {

  private List<String> 	countList;
  private Long userId;
  
  

public Long getUserId() {
	return userId;
}

public void setUserId(Long userId) {
	this.userId = userId;
}

public List<String> getCountList() {
	return countList;
}

public void setCountList(List<String> countList) {
	this.countList = countList;
}
  
}

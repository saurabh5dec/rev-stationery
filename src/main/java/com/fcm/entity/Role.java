package com.fcm.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Role {
 private static final long serialVersionUID = 1300359105852296076L;

         	@Id
         	@GeneratedValue(strategy = GenerationType.IDENTITY)

         	@Column(name = "roleId")
         	private Long roleId;
         
         	@Column(name = "name")
         	private String name;
         
         	@ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
         	private Collection<User> users;

			public Long getRoleId() {
				return roleId;
			}

			public void setRoleId(Long roleId) {
				this.roleId = roleId;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public Collection<User> getUsers() {
				return users;
			}

			public void setUsers(Collection<User> users) {
				this.users = users;
			}

			public static long getSerialversionuid() {
				return serialVersionUID;
			}

         	
         	
         	
         	
         	
         	
         	
         	
}

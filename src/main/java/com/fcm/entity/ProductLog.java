package com.fcm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="product_log")
public class ProductLog {
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//    @ManyToOne
//    private Client clients;
   @Column(name="created_on")
    private Date createdOn;
   @Column(name="unit_price")
    private Long unitPrice;
   @Column(name="updated_by")
    private String updatedBy;
   @Column(name="updated_on")
    private Date updatedOn ;
    @Column(name="status")
    private String  status;
    
    @Column(name="opertion")
    private String  opertion;
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public Date getCreatedOn() {
	return createdOn;
}
public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
}
public Long getUnitPrice() {
	return unitPrice;
}
public void setUnitPrice(Long unitPrice) {
	this.unitPrice = unitPrice;
}
public String getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}
public Date getUpdatedOn() {
	return updatedOn;
}
public void setUpdatedOn(Date updatedOn) {
	this.updatedOn = updatedOn;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getOpertion() {
	return opertion;
}
public void setOpertion(String opertion) {
	this.opertion = opertion;
}
   

}

package com.fcm.entity;


public class RequestFormResponse {
	
	  private String employeeId;
	  private String costCenter;
	  private Long quantity;
	  private Long totalQuantity;
	  private String productName;
	  private Long pendingProduct;
	
	  
	  

	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public Long getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(Long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getPendingProduct() {
		return pendingProduct;
	}
	public void setPendingProduct(Long pendingProduct) {
		this.pendingProduct = pendingProduct;
	}
	  
	  
	  

}

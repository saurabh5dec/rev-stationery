package com.fcm.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="request_form")
public class RequestForm {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name="request_id")
	    private Long id;
	    @OneToMany
		private List<RequestLog> requestLogs;
        @Column(name="created_on")
	    private String createdOn;
        @Column(name="name")
	    private String name;
        @Column(name="user_branch")
	    private String userBranch;
        @Column(name="employee_id")
	    private String employeeId;
        @Column(name="cost_center")
	    private String costCenter;
        @Column(name="quantity")
	    private Long quantity;
        @Column(name="product_name")
	    private String productName;
        @Column(name="price")
	    private Long price;
        @Column(name="updated_by")
	    private String updatedBy;
        @Column(name="updated_on")
	    private Date updatedOn ;
        
        @Column(name="admin_branch")
	    private String adminBranch;
        
        @Column(name="status")
	    private String  status ;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}


		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		

		

		public String getEmployeeId() {
			return employeeId;
		}

		public void setEmployeeId(String employeeId) {
			this.employeeId = employeeId;
		}

		public String getCostCenter() {
			return costCenter;
		}

		public void setCostCenter(String costCenter) {
			this.costCenter = costCenter;
		}

		public Long getQuantity() {
			return quantity;
		}

		public void setQuantity(Long quantity) {
			this.quantity = quantity;
		}

		public Long getPrice() {
			return price;
		}

		public void setPrice(Long price) {
			this.price = price;
		}

		public String getUpdatedBy() {
			return updatedBy;
		}

		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}

		public Date getUpdatedOn() {
			return updatedOn;
		}

		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public List<RequestLog> getRequestLogs() {
			return requestLogs;
		}

		public void setRequestLogs(List<RequestLog> requestLogs) {
			this.requestLogs = requestLogs;
		}

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public String getUserBranch() {
			return userBranch;
		}

		public void setUserBranch(String userBranch) {
			this.userBranch = userBranch;
		}

		public String getAdminBranch() {
			return adminBranch;
		}

		public void setAdminBranch(String adminBranch) {
			this.adminBranch = adminBranch;
		}
        
        
        
}

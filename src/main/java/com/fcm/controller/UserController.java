package com.fcm.controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fcm.entity.MasterAdmin;
import com.fcm.entity.Product;
import com.fcm.entity.Quantity;
import com.fcm.entity.RequestForm;
import com.fcm.entity.User;
import com.fcm.repository.MasterAdminRepository;
import com.fcm.serviceImpl.MailServiceImpl;
import com.fcm.serviceImpl.ProductServiceImpl;
import com.fcm.serviceImpl.RequestFormServiceImpl;
import com.fcm.serviceImpl.UserServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UserController {
	
	@Autowired
	RequestFormServiceImpl requestFormServiceImpl;
	@Autowired
	ProductServiceImpl productServiceImpl;
	@Autowired
	private  MailServiceImpl mailServiceImpl;
	@Autowired(required=false)
	private UserServiceImpl userServiceImpl;
	
	@Autowired
	private  MasterAdminRepository masterAdminRepository;
	
	/**
	 * this method return all applied Product data 
	 * @return
	 */
	@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
	@GetMapping(path = "/getAllApplyData/{id}", produces = "application/json")
	public ResponseEntity<List<RequestForm>> findAll(@PathVariable Long id) {
		System.out.println("enter into function");
		Optional<User> user=userServiceImpl.findByUserId(id);
        return ResponseEntity.ok(requestFormServiceImpl.findAllData(user.get().getUserCode()));
    }
/**
 * this method to update the status of applied product 
 * @param id
 * @param selectValue
 * @return
 */
	
	@PutMapping(path = "/updateStatusById/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<RequestForm> UpdateStatusById(@PathVariable Long id,
			@RequestParam(required=true,value="selectValue") String selectValue) {
		
		Optional<RequestForm> requestForm=requestFormServiceImpl.findById(id);
		if (!requestForm.isPresent())
			return ResponseEntity.notFound().build();
		  requestForm.get().setStatus(selectValue);
		  requestFormServiceImpl.save(requestForm.get());
		  String email="phoolc622@gmail.com";
		  mailServiceImpl.sendSimpleMessage(email,"","");
		return ResponseEntity.noContent().build();
    }
	
	
	/**
	 * this method to used  apply product
	 * @param countList
	 */
	 @PostMapping(path = "/addQuantity")
	 @CrossOrigin(origins = "*", maxAge = 3600)
	 @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
     public void applyRequest(@RequestBody Quantity countList,
    		 @RequestParam(required=true,value="selectValue") String selectValue) {
		 
		 Optional<User> user=userServiceImpl.findByUserId(countList.getUserId());
		 
		 RequestForm requestForm=new RequestForm();
		 try{
		 System.out.println("value of test Object"+countList.getCountList());
		List<Product> productList= productServiceImpl.findAll();
		 int index=0;
		for(Product product:productList){
			String q=countList.getCountList().get(index);
			index++;
			if(!q.equalsIgnoreCase("0")){
			System.out.println(product.getId()+""+product.getProductName()+""+"============="+q);
			requestForm.setPrice(product.getUnitPrice());
			 requestForm.setProductName(product.getProductName());
			 requestForm.setUserBranch("gurgaon");
			 requestForm.setQuantity(Long.parseLong(q));
			 requestForm.setAdminBranch(selectValue);
			 long millis=System.currentTimeMillis();  
				java.sql.Date date=new java.sql.Date(millis);
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
			    String strDate= formatter.format(date);  
			    System.out.println(strDate); 
				requestForm.setCreatedOn(strDate);
				requestForm.setStatus("pending");
				requestForm.setCostCenter(user.get().getCostCenter());
				requestForm.setEmployeeId(user.get().getUserCode());
				requestForm.setName(user.get().getEmployeeName());
			  requestFormServiceImpl.save(requestForm);
			  MasterAdmin masterAdmin=masterAdminRepository.findByBranch(selectValue);
			  String email=masterAdmin.getEmail();
			  mailServiceImpl.sendSimpleMessage(email,"Request For Stationary","One user can  applied for Product");
			}
		}
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	
	 }
}

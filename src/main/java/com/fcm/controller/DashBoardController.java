package com.fcm.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fcm.entity.Product;
import com.fcm.entity.Quantity;
import com.fcm.entity.RequestFormResponse;
import com.fcm.entity.User;
import com.fcm.serviceImpl.ProductServiceImpl;
import com.fcm.serviceImpl.RequestFormServiceImpl;
import com.fcm.serviceImpl.UserServiceImpl;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/dashboard")
public class DashBoardController {
	
	@Autowired(required=false)
	private ProductServiceImpl productServiceImpl;
	
	@Autowired(required=false)
	private UserServiceImpl userServiceImpl;
	
	@Autowired
	RequestFormServiceImpl requestFormServiceImpl;
	
   /**
    * this method return all product list on dashboard screen
    * @return
    */
	@GetMapping(path = "/getProduct", produces = "application/json")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<List<Product>> findAll() {
		System.out.println("enter into function");
        return ResponseEntity.ok(productServiceImpl.findAll());
    }
	
	/*
	 * this method used to get data from popup
	 */
	 @PostMapping(path = "/getRequestToPopUp")
	 @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	 @CrossOrigin(origins = "*", maxAge = 3600)
      public List<Product> getRequestToPopUp(@RequestBody Quantity countList) {
	    List<Product>  productlst=new ArrayList<Product>();
		 System.out.println("value of test Object"+countList.getCountList());
		  List<Product> productList= productServiceImpl.findAll();
		  int index=0;
		  for(Product product:productList){
			String q=countList.getCountList().get(index);
			index++;
			if(!q.equalsIgnoreCase("0")){
			System.out.println(product.getId()+""+product.getProductName()+""+"============="+q);
			product.setQuantity(q);
			productlst.add(product);
			}
		}
		return productlst;
	}
	 
	 
	 
	   /*
	    * this method used to get user  data by user id 
	    */
	    @GetMapping(path = "/getUserById/{id}")
		@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
		public ResponseEntity<RequestFormResponse> findUserById(@PathVariable Long id) {
	    	Optional<User> user=userServiceImpl.findByUserId(id);
			return ResponseEntity.ok( requestFormServiceImpl.findByEmpId(user.get().getUserCode()));
	    	
	    }
		/*
		 * this method is used to get last approved data and total pending products
		 */
	    @GetMapping(path = "/getApprovedAndPendingData")
		@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
		public ResponseEntity<RequestFormResponse> getApprovedAndPendingData() {
			return ResponseEntity.ok( requestFormServiceImpl.findAllReport());
	    	
	    }
	 
	    @GetMapping(path = "/getUserData/{id}")
		@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
		public User getUserData(@PathVariable Long id) {
	    	Optional<User> user=userServiceImpl.findByUserId(id);
			return user.get();
	    	
	    }
	 
}

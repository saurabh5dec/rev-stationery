package com.fcm.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fcm.entity.Product;
import com.fcm.entity.ProductLog;
import com.fcm.entity.RequestForm;
import com.fcm.serviceImpl.ProductServiceImpl;
import com.fcm.serviceImpl.RequestFormServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/product")
public class ProductController {

	
	@Autowired(required=false)
	private ProductServiceImpl productServiceImpl;
	
	@Autowired
	private RequestFormServiceImpl requestFormServiceImpl;

	
/**
 * this method to used to add a new product 
 * @param product
 * @return
 */
	    @PostMapping(path = "/addProduct")
	    @PreAuthorize("hasRole('ADMIN')")
        public ResponseEntity<Product> create(@RequestBody Product product) {
		 System.out.println("enter into add method"+product.getProductName());
        return ResponseEntity.ok(productServiceImpl.save(product));
      }
	 /**
	  * this method return all pending request for the product
	  * @return
	  */
	 
	   @GetMapping(path = "/getAllPendingRequest", produces = "application/json")
	   @PreAuthorize("hasRole('ADMIN')")
		public ResponseEntity<List<RequestForm>> findAllPendingRequest() {
		 System.out.println("enter into function");
	        return ResponseEntity.ok(requestFormServiceImpl.findAllPendingRequest());
	    }
	 
	 /**
	  * this method to used to delete the particular product 
	  * @param id
	  */
	   @DeleteMapping(path = "/deleteById/{id}")
	   @PreAuthorize("hasRole('ADMIN')")
	    public void deleteById(@PathVariable Long id) {
		     List<ProductLog> lstproductLogs=new ArrayList<ProductLog>();
			 System.out.println("enter into add method");
			
			 Optional<Product> product=productServiceImpl.findById(id);
			 
			 product.get().getProductLog().get(0).getId();
			 
			 Optional<ProductLog>  productLog=productServiceImpl.findByProductLogIdByProductId(product.get().getProductLog().get(0).getId());
			 product.get().setStatus("inActive");
			 productLog.get().setStatus("inActive");
			 productLog.get().setOpertion("delete");
			 long millis=System.currentTimeMillis();  
				java.sql.Date date=new java.sql.Date(millis); 
				productLog.get().setCreatedOn(date);
				productLog.get().setUpdatedBy("admin");
				productLog.get().setUpdatedOn(date);
			 lstproductLogs.add(productLog.get());
			 productServiceImpl.update(product.get());
			
	   }
	   /**
	    * this method used to edit the product
	    * @param product
	    * @param id
	    * @return
	    */
	   @PutMapping(path = "/editProduct/{id}")
	   @PreAuthorize("hasRole('ADMIN')")
       public Product editProduct(@RequestBody Product product,@PathVariable Long id) {
		   Product products=null;
		   List<ProductLog> lstproductLogs=new ArrayList<ProductLog>();
		   Optional<Product> productData=productServiceImpl.findById(id);
		   if(productData.isPresent()){
			   productData.get().getProductLog().get(0).getId();

			Optional<ProductLog>  productLog=productServiceImpl.findByProductLogIdByProductId(productData.get().getProductLog().get(0).getId());
			   productData.get().setProductName(product.getProductName());
			   productData.get().setUnitPrice(product.getUnitPrice());
			   productData.get().setUpdatedBy("admin");
			   long millis=System.currentTimeMillis();  
				java.sql.Date date=new java.sql.Date(millis);
			   productData.get().setUpdatedOn(date);
			   productLog.get().setCreatedOn(date);
				productLog.get().setUpdatedBy("admin");
				productLog.get().setUpdatedOn(date);
				productLog.get().setOpertion("edit");
			 lstproductLogs.add(productLog.get());
			 products= productServiceImpl.edit(productData.get());   
		   }
		return products;
		   
     }
	 

}
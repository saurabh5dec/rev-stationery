package com.fcm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fcm.serviceImpl.ExcelServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/excel")
public class ExcelController {
	
	  @Autowired
	  ExcelServiceImpl excelServiceImpl;
	
	 @GetMapping("/download")
	  public ResponseEntity<Resource> getFile() {
	    String filename = "Report.xlsx";
	    InputStreamResource file = new InputStreamResource(excelServiceImpl.load());

	    return ResponseEntity.ok()
	        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
	        .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
	        .body(file);
	  }
	 
	 
	 @GetMapping("/download/pending")
	  public ResponseEntity<Resource> getPendingFile() {
	    String filename = "Report.xlsx";
	    InputStreamResource file = new InputStreamResource(excelServiceImpl.loadPending());

	    return ResponseEntity.ok()
	        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
	        .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
	        .body(file);
	  }
}



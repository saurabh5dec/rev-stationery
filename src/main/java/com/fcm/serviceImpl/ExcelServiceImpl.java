package com.fcm.serviceImpl;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fcm.entity.RequestForm;
import com.fcm.repository.RequestFormRepository;
import com.fcm.service.ExcelHelper;
import com.fcm.service.ExcelService;

@Service
public class ExcelServiceImpl implements ExcelService {
		// TODO Auto-generated method stub
		 @Autowired
		  RequestFormRepository requestFormRepository;

		  public ByteArrayInputStream load() {
		    List<RequestForm> requestForms = requestFormRepository.findAll();

		    ByteArrayInputStream in = ExcelHelper.tutorialsToExcel(requestForms);
		    return in;
		  }
		  
		  public ByteArrayInputStream loadPending() {
			  List<RequestForm> requestFormList=new ArrayList<RequestForm>();
		    List<RequestForm> requestForms = requestFormRepository.findAll();
		    for(RequestForm requestForm:requestForms){
              if(requestForm.getStatus().equalsIgnoreCase("pending"))
            	  requestFormList.add(requestForm);
		    }
		    ByteArrayInputStream in = ExcelHelper.tutorialsToExcel(requestFormList);
		    return in;
		  }
		  
}

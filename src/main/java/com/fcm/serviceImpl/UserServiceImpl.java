package com.fcm.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fcm.entity.User;
import com.fcm.repository.UserRepository;
import com.fcm.service.UserService;
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	
	@Override
	public Optional<User> findByUserId(Long id) {
		// TODO Auto-generated method stub
		System.out.println("value of id"+id);
		return userRepository.findById(id);
	}


	
	

}

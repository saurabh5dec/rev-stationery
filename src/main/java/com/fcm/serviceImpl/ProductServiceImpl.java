package com.fcm.serviceImpl;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fcm.entity.Product;
import com.fcm.entity.ProductLog;
import com.fcm.repository.ProductLogRepository;
import com.fcm.repository.ProductRepository;
import com.fcm.service.ProductService;
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;
	@Autowired
	ProductLogRepository productLogRepository;
	
	

	@Override
	public List<Product> findAll()  {
		// TODO Auto-generated method stub
		List<Product> pList=new ArrayList<>();
	   List<Product> productList=productRepository.findAll();
	   try{
	    for(Product product:productList){
	    	if(product.getStatus().equalsIgnoreCase("active")){

	    		pList.add(product);
	    }
	   }
	   }catch(Exception e){
		   e.printStackTrace();
	   }
		return pList;
	}

	public Product save(Product product) {
		Product newProduct=new Product();
		List<ProductLog> pList=new ArrayList<>();
		if(product.getProductName()!=null){
		newProduct.setProductName(product.getProductName());
		newProduct.setUnitPrice(product.getUnitPrice());
		newProduct.setStatus("active");
		newProduct.setUpdatedBy("admin");
		newProduct.setQuantity("0");
		long millis=System.currentTimeMillis();  
		java.sql.Date date=new java.sql.Date(millis); 
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
	    String strDate= formatter.format(date);  
	    System.out.println(strDate); 
			newProduct.setCreatedOn(strDate);
			ProductLog productLog=new ProductLog();
			productLog.setStatus("active");
			productLog.setUnitPrice(product.getUnitPrice());
			productLog.setCreatedOn(date);
			productLog.setOpertion("insert");
			pList.add(productLog);
			newProduct.setProductLog(pList);
			newProduct=productRepository.save(newProduct);
		}
		return	newProduct;
			
	}

	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	         productRepository.deleteById(id);
	}

	public Optional<Product> findById(Long id) {
		// TODO Auto-generated method stub
		return productRepository.findById(id);
	}

	public void update(Product product) {
		// TODO Auto-generated method stub
		productRepository.save(product);
	}

	public Optional<ProductLog> findByProductLogIdByProductId(Long id) {
		// TODO Auto-generated method stub
		return productLogRepository.findById(id);
	}

	public Product edit(Product product) {
		return productRepository.save(product);
	}

	

}

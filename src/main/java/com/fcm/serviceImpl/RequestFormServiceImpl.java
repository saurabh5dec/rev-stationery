package com.fcm.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fcm.entity.RequestForm;
import com.fcm.entity.RequestFormResponse;
import com.fcm.entity.User;
import com.fcm.repository.RequestFormRepository;
import com.fcm.service.RequestFormService;

@Service
public class RequestFormServiceImpl implements RequestFormService{

	
     @Autowired
     RequestFormRepository requestFormRepository;
     

	@Override
	public List<RequestForm> findAllData(String empId) {
		// TODO Auto-generated method stub
		return requestFormRepository.findByemployeeId(empId);
	}

	public List<RequestForm> findAllPendingRequest() {
		// TODO Auto-generated method stub
		
		List<RequestForm> requestFormlst=new ArrayList<>();
		
		List<RequestForm> requestFormList= requestFormRepository.findAll();
		for(RequestForm requestForm:requestFormList){
			if(requestForm.getStatus().equalsIgnoreCase("pending")){	
				requestFormlst.add(requestForm);
			}
		}
		
		return requestFormlst;
	}

	public Optional<RequestForm> findById(Long id) {
		// TODO Auto-generated method stub
		return requestFormRepository.findById(id);
	}

	public void save(RequestForm requestForm) {
		// TODO Auto-generated method stub
		requestFormRepository.save(requestForm);
	}

	public RequestFormResponse findByEmpId(String empId) {
		// TODO Auto-generated method stub
		RequestFormResponse newRequestFormResponse=new RequestFormResponse();
	    List<RequestForm>	requestForms= requestFormRepository.findByemployeeId(empId);
	    long totalQuantity = 0;
	    for(RequestForm requestForm:requestForms){
	    	totalQuantity=totalQuantity +requestForm.getQuantity();
	    	newRequestFormResponse.setTotalQuantity(totalQuantity);
	    }
	    RequestForm  requestForm =requestForms.get(requestForms.size() -1);
	      
	      newRequestFormResponse.setQuantity(requestForm.getQuantity());
	      newRequestFormResponse.setProductName(requestForm.getProductName());
		 return newRequestFormResponse;
	}

	public RequestFormResponse findAllReport() {
		// TODO Auto-generated method stub
		RequestFormResponse newRequestFormResponse=new RequestFormResponse();
		List<RequestForm> requestFormlst=new ArrayList<>();
		List<RequestForm> requestFormList= requestFormRepository.findAll();
		long pendingProduct = 0;
		for(RequestForm requestForm:requestFormList){
			if(requestForm.getStatus().equalsIgnoreCase("Done")){	
				requestFormlst.add(requestForm);
			}else{
				if(requestForm.getStatus().equalsIgnoreCase("Pending")){
					pendingProduct++;	
				}
			}
		}
		 RequestForm  requestForm =requestFormlst.get(requestFormlst.size() -1);
		  newRequestFormResponse.setPendingProduct(pendingProduct);
	      newRequestFormResponse.setQuantity(requestForm.getQuantity());
	      newRequestFormResponse.setProductName(requestForm.getProductName());
		
		return newRequestFormResponse;
	}



}

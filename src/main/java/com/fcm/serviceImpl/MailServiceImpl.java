package com.fcm.serviceImpl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.fcm.service.MailService;

@Service
public class MailServiceImpl implements MailService {

	    @Autowired
	    JavaMailSender mailSender;
	 
	  
	     public void sendSimpleMessage(String email,String subject,String text) {
	    	 MimeMessage message = mailSender.createMimeMessage();
		        MimeMessageHelper helper = new MimeMessageHelper(message);

		        try {
		        	helper.setFrom("no.reply@in.fcm.travel");
		            helper.setTo(email);
		            helper.setText(text);
		            helper.setSubject(subject);
		        } catch (MessagingException e) {
		            e.printStackTrace();
		        }
		        mailSender.send(message);
		    }
	 
	}
